
#### Stress

The program is essentially trying out multiple ways of comparing the different samples/plots. In the end, it selects the best possible way to distinguish between all your samples. But how good is 'the best way'? That is where stress comes in to play. Stress is an indication for the goodness of fit, a measure between the observed similarity versus the expected similarity. If that sounds a bit vague, all you have to know is: A lower stress value means a more accurate comparison. 

|Stress value|Conclusion|
|--------------|-----------------------------|
| 20% or above | Very Poor (not worth doing) |
| 10%-19.9% | Fair |
| 5%-9.9% | Good |
| 2.5%-4.9% | Excellent |
| 0% - 2.4% | Near Perfect Fit |
