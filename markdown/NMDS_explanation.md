# Non-metric multidimensional scaling (NMDS)

One way to represent beta diversity is by using Non-metric Multidimensional Scaling
(NMDS) plots. NMDS is a means of visualizing the level of similarity among individual 
samples and it aims at representing the original position of collected data in a
2D space.
