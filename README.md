# FoodWebs

## Publish on shiny.wur.nl
To publish this application on shiny.wur.nl, the following steps are needed:
 
 1. Make sure your account has the right to publish on https://shiny.wur.nl/ (you can request that on the website itself)
 1. Install the Rstudio IDE on your computer (see https://posit.co/products/open-source/rstudio/)
 1. Link RStudio to shiny.wur.nl (see https://shiny.wur.nl/__docs__/user/connecting/)
 1. Then load your code into RStudio (open the Webs2020.Rproj file), and look for the blue publish icon if you have opened the ui.R or server.R file.
 1. After that you can make set the access to the application on the website. If you can't find your application, the easiest route is through your profile, which you can access by clicking on your name in the top right corner.

If there are errors while deploying, take a good look at the errors, usually they give a clue what needs to be fixed.
Things that might need fixing:
 1. Update the libraries in your client (Tools --> Check for Package Updates)
 1. Ask the admins to install missing system libraries on the shiny.wur.nl server. (https://support.wur.nl/, "R-Studio Connect (shiny.wur.nl)" is the service)
 1. Be explicit in your library imports, the packer can only find dependancies when you help it.
