library(shiny)
library(DT)
library(shinythemes)
library(magrittr)
library(shinydashboard)
library(stringr)
library(vegan)
library(reshape2)
library(ggplot2)
library(dplyr)
library(vegan)
library(igraph)
library(SpiecEasi)

#source("Functions.R")

Get_alpha_diversity <- function(bio){
  
  bio %>% mutate_all(as.numeric)
  bio1 <- as.matrix(bio)
  
  
  #shannon
  H <- vegan::diversity(bio1)
  
  # Observed Richness
  richness <- specnumber(bio1)  
  
  # Pielou's Evenness
  evenness <- H/log(richness)
  
  
  # Create alpha diversity dataframe including environmental data
  alpha <- cbind(sample = rownames(bio1), shannon = H, richness = richness, pielou = evenness)
  
  
  
  return(alpha)
  
  
  
  
  
  
  
}


server <- function(input, output, session) {
    

    student_data <- reactiveVal(NULL)
    MDS_data <- reactiveVal(NULL)
    env_fit <- reactiveVal(NULL)
    
    env_factors <- reactiveVal(NULL)
    biotic_factors <- reactiveVal(NULL)
    
    observeEvent(input$file, {
        student_data(import_data())
        

    })
    
    observeEvent(input$sep,{
        student_data(import_data())

    })


    
    observeEvent(input$input_dataset_columns_selected,{
      req(student_data())
      
      abiotic_selection <- input$input_dataset_columns_selected
      env_factors(student_data()[abiotic_selection])
      biotic_factors(student_data()[-abiotic_selection])
      

      
      tryCatch(
        {
          MDS_data(run_MDS())
          
        },
        error = function(cond){
          print(cond)
        }
      )
    })
    

    


    output$input_dataset <- DT::renderDataTable({
        req(input$file)
    
        return(DT::datatable(student_data(), selection = list(target = "column"), options = list(ordering = FALSE, searching = FALSE)))
    })
    

    
    output$Variable_select <- renderUI({
      req(student_data())
      abiotic_allgroups <- colnames(env_factors())
      selectInput('selection_input', label = 'Select label', choices = abiotic_allgroups)
    })
    
    
    output$linearmodel_radio <- renderUI({
      req(student_data())
      
      abiotic_allgroups <- colnames(env_factors())
      radioButtons('LM_model_alpha_inputvar', label = 'Select label', choices = abiotic_allgroups, selected = 'Treatment')
    })
    
    output$Env_factors_select <- renderUI({
      req(student_data())

      abiotic_allgroups <- colnames(env_factors())


      checkboxGroupInput(inputId = 'environmental_factors', label = 'Select environmental factors',choices = abiotic_allgroups)
    })
    
    output$MDS_stress_plt <- renderPlot({
        req(student_data())
        req(MDS_data())
        
        out <- tryCatch(
          {
            MDS_log <- run_MDS(TRUE)
            
            stress_vals <- str_extract_all(MDS_log, regex(pattern = '0[.][0-9]+')) %>% 
              unlist %>%  
              as.numeric 
            
            
            ##note the stress, this value reflects goodness of fit: stress > 0.3 = poor fit, stress 0.2 = fairly good fit, stress = 0.1 or lower = good fit 
            plot(stress_vals, type = 'b', xlab = "Run", ylab = "Stress", ylim = c(0,1), main = "MDS stress")
            
            
            abline(h = 0.2)
            text(x = 5, y = 0.23, "Upper limmit (> 20%)")
            
            abline(h = 0.1)
            text(x = 5.5, y = 0.13, "Good fit (this and lower)")
            
          },
          error = function(cond){
            return(plot(0,type='n',axes=FALSE,ann=FALSE))
          }
        )
        
        
    })
    
    output$MDS_composition_plt <- renderPlot({
        req(student_data())
        req(MDS_data())
        
        data <- student_data()
        var_names <- data %>% colnames
        selection <- input$input_colnames
        
        
        biotic_allgroups<- biotic_factors()
        abiotic_allgroups <- env_factors()

        plot(MDS_data(),  display="sites")
        
        for(show in unique(abiotic_allgroups[,input$selection_input])){ 
          ordihull(MDS_data(), group=abiotic_allgroups[,input$selection_input], 
                   show=show, col="red", 
                   lty=1, cex=.8, label=TRUE)    
        }
        
        if(! is.null(input$environmental_factors)){
          
          env_data <- student_data()[input$environmental_factors]
          env_data %<>% apply(., 2, function(x){
            gsub(',','.',x) %>% as.numeric
          }) 
          
          env_fit(envfit(MDS_data(), env_data , permu=1000))
          plot(env_fit())  
        }

        ##add taxon names, position taxon reflects their correlation with a given plot type or environmental factor
        stems = colSums(biotic_allgroups)
        taxonnames<-orditorp(MDS_data(), dis="species", priority = stems, col="grey",pch="", cex=.8)

    })

    output$MDS_vector <- renderTable({
      req(env_fit())
      data <- env_fit()
      
      tabl <- cbind(data$vectors$arrows, data$vectors$r, data$vectors$pvals)
      colnames(tabl) <- c("NMDS1","NMDS2","R",'Pvalue')
      rownames(tabl) <- rownames(data$vectors$arrows)
      
      return(tabl)
    }, rownames = TRUE, bordered = TRUE)
    
    
    
    output$shannon_div_plt <- renderPlot({
      req(biotic_factors())
      
      biotic_allgroups<- biotic_factors()
      env <- env_factors()
      

      
      alpha <- Get_alpha_diversity(biotic_allgroups)

      alpha <- cbind(alpha, env)

      
     
     
      print(colnames(alpha))
   
      # ymin = min(alpha$sample, na.rm = TRUE)
       #ymax = max(alpha$sample, na.rm = TRUE)
 
      ggplot(alpha, aes(x = sample, y = shannon, colour = sample)) +
        geom_point(size = 3) +
        ylab("Shannon's H'") + 
        xlab("") +
        theme_bw() +
        ylim(0, 2) +
        theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.4))
      
      
    })
    
    output$LM_alpha <- renderPlot({
      req(biotic_factors())
      
      biotic_allgroups<- biotic_factors()
      env <- env_factors()
      
      selected_variable <- input$LM_model_alpha_inputvar
  
      
      
      alpha <- Get_alpha_diversity(biotic_allgroups)
      

      alpha <- cbind(alpha, env)
      
      
      ggplot(alpha, aes_string(x = selected_variable, y = "shannon", colour = "sample")) +
        geom_point(size = 3) +
        xlab(selected_variable) + 
        ylab("Shannon's H'") +
         ylim(0,2) +
        theme_bw()
      
    })
    
    
    output$plt_cluster_dendro <- renderPlot({
      req(biotic_factors())
      
      biotic_allgroups<- biotic_factors()
      
      dis <- vegdist(biotic_allgroups)
      clus <- hclust(dis, "single")#single linkage = nearest neighbour, complete linkage = furthest neighbour, average = compromise between the previous two extremes
      plot(clus)
    })
    
    run_MDS <- function(out_log = F){
      
      biotic_allgroups<- biotic_factors()
      
      log <- capture.output(mds <-metaMDS(biotic_allgroups,"bray", permu=1000, zerodist="add", noshare=0))
      
      if(out_log){ return(log) }
      else{ return(mds) }  
      
    }
    
    import_data <- function(){
      req(input$file)
      
      data <- read.csv(input$file$datapath, sep = input$sep, stringsAsFactors = F)
      colnames(data)[1] <- "sample"
      
      return(data)
    }
    
    
    output$plt_Network <- renderPlot({
      req(biotic_factors())
      
      biotic_allgroups<- biotic_factors()
      environmentaldata <- env_factors()
      
      bio <- data.frame(biotic_allgroups)
      bio.sparcc <- sparcc(bio, iter = 20, inner_iter = 10, th = 0.1)
      sparcc.cutoff   <- input$slider_groupThreshold
      bio.sparcc.adj <- ifelse(abs(bio.sparcc$Cor) >= sparcc.cutoff, 1, 0)
      rownames(bio.sparcc.adj) <- colnames(bio)
      colnames(bio.sparcc.adj) <- colnames(bio)
      
      bio.sparcc.adj.ig <- graph.adjacency(bio.sparcc.adj, mode = "undirected", diag = FALSE)
      co <- layout_with_fr(bio.sparcc.adj.ig, niter = 2000)
      
      plot.igraph(bio.sparcc.adj.ig, layout = co, asp = 0, 
                  main = "Network analysis", sub = "Correlation between biotic groups",
                  ## nodes =======================================
                  vertex.label  = as.character(colnames(biotic_allgroups)),
                  vertex.color  = as.factor(environmentaldata$Treatment),
                  vertex.size   = 5,
                  ## edges =======================================
                  edge.color = "lightgray",
                  edge.width = 5,               ## default = 1
                  edge.lty = "solid",           ## linetype: blank, solid, dashed, dotted,
                  ## dotdash, longdash, or twodash
                  edge.curved = 0.05,            ## 0 to 1 or TRUE (0.5)
                  height = 400, width = 600
      )
      
    })
    
}

shinyServer(server)
