#spar CC

Alldata <-read.csv("WEBS/dummydataPyr.csv", dec=',', sep=';', header=TRUE)
###select columns with biotic data###
biotic_allgroups<- (Alldata[5:56])

###select columns with abiotic data and environmental data (=treatment description, abiotics)###
environmentaldata <- Alldata[2:4]

## sparcc
library(SpiecEasi)
bio <- data.frame(biotic_allgroups)

bio.sparcc <- sparcc(bio, iter = 20, inner_iter = 10, th = 0.1)
#boxplot(bio.sparcc)

sparcc.cutoff   <- 0.7
bio.sparcc.adj <- ifelse(abs(bio.sparcc$Cor) >= sparcc.cutoff, 1, 0)
## Add OTU names to rows and columns
rownames(bio.sparcc.adj) <- colnames(bio)
colnames(bio.sparcc.adj) <- colnames(bio)

library(igraph)
library(plotly)
## Build network from adjacency
bio.sparcc.adj.ig <- graph.adjacency(bio.sparcc.adj, mode = "undirected", diag = FALSE)
co <- layout_with_fr(bio.sparcc.adj.ig, niter = 2000)

plot.igraph(bio.sparcc.adj.ig, layout = co, asp = 0, 
            main = "Network analysis", sub = "Correlation between biotic groups",
            ## nodes =======================================
            vertex.label  = as.character(colnames(biotic_allgroups)),
            vertex.color  = as.factor(environmentaldata$Treatment),
            vertex.size   = 5,
            ## edges =======================================
            edge.color = "lightgray",
            edge.width = 5,               ## default = 1
            edge.lty = "solid",           ## linetype: blank, solid, dashed, dotted,
            ## dotdash, longdash, or twodash
            edge.curved = 0.05            ## 0 to 1 or TRUE (0.5)
)
